//
//  ViewController.swift
//  IOSTask1
//
//  Created by Zolo on 01/02/23.
//

import UIKit
let students :[String] = ["Sachin","Aryan","Viral","Srishti","Aashi","Prajjwal","Kartik","Anurag"]


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate=self
        tableView.dataSource=self
    }
    
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return students.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell :TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
            cell.labelName.text = students[indexPath.row]
            
            return cell
            
        }
}

