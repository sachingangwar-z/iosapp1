//
//  TableViewCell.swift
//  IOSTask1
//
//  Created by Zolo on 01/02/23.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    
    @IBAction func addButton(_ sender: Any) {
        print(labelName.text!)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
